//Copyright(c) 2013
//Authors: Fabian Wahlster, Steffen Wiewel, Magdalena Papagianni
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com
//License: Read LICENSE.txt located in the root directory

.globl _fast_inv_sqrt
.globl _fast_inv_sqrt_newton
.globl _faster_inv_sqrt
.globl _faster_inv_sqrt_newton

.code 32
//r0 magicvalue
//r1 input float vector
//r2 output float vector
//r3 input length

## void _fast_inv_sqrt_newton
_fast_inv_sqrt_newton:
	#full decending, push all callee-save registers
    stmfd   sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}

	#magicvalue in q0 laden
	vmov.f32 s0,r0
	vmov.f32 s1,r0
	vmov.f32 s2,r0
	vmov.f32 s3,r0
	#Konstanten f�r Multiplikation der Newton-Iteration in d15[0] und d15[1]
	vmov.f32 s30,#0.5
	vmov.f32 s31,#1.5
n_loop:
	cmp r3,#12
	#springe wenn weniger als 12 floats zu lesen sind
	blt n_less12
	#die ersten 12 floats laden
 	vld1.32  {q1,q2}, [r1]!
 	vld1.32  {q3}, [r1]!

	#x durch 2 teilen => mul mit 0.5 ist schneller als vdif // f�r xhalf der Newton-Iteration
 	vmul.f32 q4,q1,d15[0]
 	vmul.f32 q5,q2,d15[0]
 	vmul.f32 q6,q3,d15[0]

	# x >> 1
	vshr.s32 q1,q1,#1
	vshr.s32 q2,q2,#1
	vshr.s32 q3,q3,#1

	#magic - (x >> 1)
	vsub.i32 q1,q0,q1
	vsub.i32 q2,q0,q2
	vsub.i32 q3,q0,q3

	#newton-iteration
	#xhalf * x
	vmul.f32 q4,q4,q1
	vmul.f32 q5,q5,q2
	vmul.f32 q6,q6,q3

	#xhalf * x * x
	vmul.f32 q4,q4,q1
	vmul.f32 q5,q5,q2
	vmul.f32 q6,q6,q3

	#xhalf * x * x * x
	vmul.f32 q4,q4,q1
	vmul.f32 q5,q5,q2
	vmul.f32 q6,q6,q3

	#x * 1.5
	vmul.f32 q1,q1,d15[1]
	vmul.f32 q2,q2,d15[1]
	vmul.f32 q3,q3,d15[1]

	# x * 1.5 - xhalf*x*x*x;
	vsub.f32 q1,q1,q4
	vsub.f32 q2,q2,q5
	vsub.f32 q3,q3,q6

	#Ergebnisse in output array speichern
	vst1.32  {q1,q2}, [r2]!
 	vst1.32  {q3}, [r2]!

	sub r3,#12
	b n_loop
n_less12:
	cmp r3,#4
	#springen wenn weinger als 4 floats zu lesen sind
	blt n_less4
	vld1.32  {q1}, [r1]!

	#xhalf f�r Newton-Iteraion
	vmul.f32 q2,q1,d15[0]

	#magic - (x >> 1)
	vshr.s32 q1,q1,#1
	vsub.i32 q1,q0,q1

	#xhalf * x * x * x
	vmul.f32 q2,q2,q1
	vmul.f32 q2,q2,q1
	vmul.f32 q2,q2,q1

	#x * 1.5
	vmul.f32 q1,q1,d15[1]

	# x * 1.5 - xhalf*x*x*x;
	vsub.f32 q1,q1,q2

	#ergebniss speichern
	vst1.32 {q1}, [r2]!
	sub r3,#4
	#schleife wiederholen
	b n_less12
n_less4:
	cmp r3,#2
	blt n_less2
	vld1.32  d2, [r1]!

	#divide x by 2 => mul 0.5 s30 => xhalf
	vmul.f32 d3,d2,d15[0]

	#magic - (x >> 1)
	vshr.s32 d2,d2,#1
	vsub.i32 d2,d0,d2

	#xhalf * x * x * x
	vmul.f32 d3,d3,d2
	vmul.f32 d3,d3,d2
	vmul.f32 d3,d3,d2

	#x * 1.5
	vmul.f32 d2,d2,d15[1]

	# x * 1.5 - xhalf*x*x*x;
	vsub.f32 d2,d2,d3

	vst1.32 d2, [r2]!
	sub r3,#2
n_less2:
	cmp r3,#1
	#springe zum ende wenn keine daten mehr verf�gbar sind
	blt n_exit
	#lade das letzte verbleibende float
	vld1.32  d2[0], [r1]!

	# mul 0.5 s30 => xhalf
	vmul.f32 d3,d2,d15[0]

	#magic - (x >> 1)
	vshr.s32 d2,d2,#1
	vsub.i32 d2,d0,d2

	#xhalf * x * x * x
	vmul.f32 d3,d3,d2
	vmul.f32 d3,d3,d2
	vmul.f32 d3,d3,d2

	#x * 1.5
	vmul.f32 d2,d2,d15[1]

	# x * 1.5 - xhalf*x*x*x;
	vsub.f32 d2,d2,d3

	#ergebniss speichern
	vst1.32 d2[0], [r2]!

    #exit function
n_exit:
    ldmfd   sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}

_fast_inv_sqrt:
	#full decending, push all callee-save registers
    stmfd   sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}

#load magicvalue to q0
	vmov.f32 s0,r0
	vmov.f32 s1,r0
	vmov.f32 s2,r0
	vmov.f32 s3,r0
loop:
	cmp r3,#28
	blt less28
	# 28 floats in q1-q7 laden
 	vld1.32  {q1,q2}, [r1]!
 	vld1.32  {q3,q4}, [r1]!
 	vld1.32  {q5,q6}, [r1]!
 	vld1.32  {q7}, [r1]!

	# x >> 1
	vshr.s32 q1,q1,#1
	vshr.s32 q2,q2,#1
	vshr.s32 q3,q3,#1
	vshr.s32 q4,q4,#1
	vshr.s32 q5,q5,#1
	vshr.s32 q6,q6,#1
	vshr.s32 q7,q7,#1

	# magic - (x >> 1)
	vsub.i32 q1,q0,q1
	vsub.i32 q2,q0,q2
	vsub.i32 q3,q0,q3
	vsub.i32 q4,q0,q4
	vsub.i32 q5,q0,q5
	vsub.i32 q6,q0,q6
	vsub.i32 q7,q0,q7

	# speichern der 28 ergeniss
	vst1.32  {q1,q2}, [r2]!
 	vst1.32  {q3,q4}, [r2]!
 	vst1.32  {q5,q6}, [r2]!
 	vst1.32  {q7}, [r2]!

	# 28 vom counter subtrahieren um verbleibende floats zu erhalten
	sub r3,#28
	b loop
less28:
	cmp r3,#4
	blt less4
	# 4 floats laden
	vld1.32  {q1}, [r1]!

	# x >> 1
	vshr.s32 q1,q1,#1

	# magic - (x >> 1)
	vsub.i32 q1,q0,q1

	# 4 floats speichern
	vst1.32 {q1}, [r2]!

	sub r3,#4
	b less28
less4:
	cmp r3,#2
	blt less2
	# 2 floats laden
	vld1.32  d2, [r1]!

	# x >> 1
	vshr.s32 d2,d2,#1

	# magic - (x >> 1)
	vsub.i32 d2,d0,d2
	vst1.32 d2, [r2]!
	sub r3,#2
less2:
	cmp r3,#1
	blt exit
	# ein float laden
	ldr ip,[r1]!
	# x >> 1
	lsr ip,ip,#1
	# magic - (x >> 1)
	sub ip,r0,ip
	# float speichern
	str ip,[r2]!

    #exit function
exit:

    ldmfd   sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}

//r0 input float vector
//r1 output float vector
//r2 input length

_faster_inv_sqrt:

	stmfd   sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}

	//Um Vergleichbarkeit mit _fast_inv_sqrt zu gew�hrleisten werden hier nur 28 Floats eingelesen (statt der m�glichen 32)
f_loop:
	cmp r2,#28
	blt f_less28

	# 28 floats laden
 	vld1.32  {q0,q1}, [r0]!
 	vld1.32  {q2,q3}, [r0]!
 	vld1.32  {q4,q5}, [r0]!
 	vld1.32  {q6}, [r0]!

	# InvSqrt estimate neon funktion // magic - (x >> 1), hier wahrscheinlich via LoopTable realisiert
	vrsqrte.f32 q0, q0
	vrsqrte.f32 q1, q1
	vrsqrte.f32 q2, q2
	vrsqrte.f32 q3, q3
	vrsqrte.f32 q4, q4
	vrsqrte.f32 q5, q5
	vrsqrte.f32 q6, q6

	# 28 floats speichern
	vst1.32  {q0,q1}, [r1]!
 	vst1.32  {q2,q3}, [r1]!
 	vst1.32  {q4,q5}, [r1]!
 	vst1.32  {q6}, [r1]!

	sub r2,#28
	b f_loop
f_less28:
	cmp r2,#4
	blt f_less4
	vld1.32  {q0}, [r0]!
	vrsqrte.f32 q0, q0
	vst1.32 {q0}, [r1]!
	sub r2,#4
	b f_less28
f_less4:
	cmp r2,#2
	blt f_less2
	vld1.32  d0, [r0]!
	vrsqrte.f32 d0, d0
	vst1.32 d0, [r1]!
	sub r2,#2
f_less2:
	cmp r2,#1
	blt f_exit
	vldr.f32 s0, [r0]
	vrsqrte.f32 d0, d0
	vstr.f32 s0, [r1]

    #exit function
f_exit:

    ldmfd   sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}

    //r0 input float vector
	//r1 output float vector
	//r2 input length

_faster_inv_sqrt_newton:

	stmfd   sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}

	//Um Vergleichbarkeit mit _fast_inv_sqrt_newton zu gew�hrleisten werden hier nur 12 Floats eingelesen (statt der m�glichen 32)
n_f_loop:
	cmp r2,#12
	blt n_f_less12

	# lade 12 floads
 	vld1.32  {q0,q1}, [r0]!
 	vld1.32  {q2}, [r0]!

	# InvSqrt estimate neon funktion
	vrsqrte.f32 q0, q0
	vrsqrte.f32 q1, q1
	vrsqrte.f32 q2, q2

	# InvSqrt step neon funktion // Newton-Iteration
	vrsqrts.f32 q0, q0
	vrsqrts.f32 q1, q1
	vrsqrts.f32 q2, q2

	# speichere 12 floats
	vst1.32  {q0,q1}, [r1]!
 	vst1.32  {q2}, [r1]!

	sub r2,#12
	b n_f_loop
n_f_less12:
	cmp r2,#4
	#weier wenn weniger als 4 floats �brig sind
	blt n_f_less4
	# 4 floats laden
	vld1.32  {q0}, [r0]!
	#neon estimate funtktion
	vrsqrte.f32 q0, q0
	#neon step funktion
	vrsqrts.f32 q0, q0
	# floats sreichern
	vst1.32 {q0}, [r1]!
	#verbleibende anzahl an floats errechnen
	sub r2,#4
	b n_f_less12
n_f_less4:
	cmp r2,#2
	blt n_f_less2
	# 2 floats laden
	vld1.32  d0, [r0]!
	# neon estimate
	vrsqrte.f32 d0, d0
	# neon step
	vrsqrts.f32 d0, d0
	# 2 floats speichern
	vst1.32 d0, [r1]!
	sub r2,#2
n_f_less2:
	cmp r2,#1
	blt n_f_exit
	#float laden
	vldr.f32 s0, [r0]
	# neon estimate
	vrsqrte.f32 d0, d0
	# neon step
	vrsqrts.f32 d0, d0
	#float speichern
	vstr.f32 s0, [r1]

    #exit function
n_f_exit:

    ldmfd   sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}

