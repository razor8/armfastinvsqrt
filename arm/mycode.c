//Copyright(c) 2013
//Authors: Fabian Wahlster, Steffen Wiewel, Magdalena Papagianni
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com
//License: Read LICENSE.txt located in the root directory

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>

extern void _fast_inv_sqrt(int magic,float* input,float *output, int count);
extern void _faster_inv_sqrt(float* input,float *output, int count);

extern void _fast_inv_sqrt_newton(int magic,float* input,float *output, int count);
extern void _faster_inv_sqrt_newton(float* input,float *output, int count);

//Original C Algorithmus
float fastInvSqrtNewton(int magic, float x) {
	int i = *(int*)&x;
	float xhalf = x*0.5f;
	i = magic - (i>>1);
	x = *(float*)&i;
	x = x*(1.5f - xhalf*x*x);
	return x;
}

//Original C Algorithmus ohne Newton-Iteration
float fastInvSqrt(int magic, float x) {
	int i = *(int*)&x;
	i = magic - (i>>1);
	x = *(float*)&i;
	return x;
}
//Referenz C Algorithmus
float InvSqrt(float x){
	return 1.f/sqrtf(x);
}

int main(int argc, char *argv[]) {
	int magic = 0x5F3970D6;
	int newton_magic = 0x5F387D4A;

	int count = argc -1;
	float *input;
	float *output;
	if(argc > 1){
		input = malloc(count*sizeof(float));
		output = malloc(count*sizeof(float));

		for(int i = 0;i<count;++i){
			input[i] = (float)atof(argv[i+1]);
			//printf("Eingabe %.8f Ausgabe %.8f\n",input[i],fastInvSqrtNewton(magic,input[i]));
		}

		_fast_inv_sqrt_newton(newton_magic,input,output,count);

		for(int i = 0;i<count;++i){
			printf("Eingabe %.8f Ausgabe %.8f\n",input[i],output[i]);
		}

		return 0;
	}

	//Variablen f�r die Zeitmessung
	struct timeval tv1,tv2;
	double cinvsqrt = 0, cfastinvsqrt = 0, armfastinvsqrt = 0, armfasterinvsqrt = 0;
	double cinvsqrt_newton = 0, cfastinvsqrt_newton = 0, armfastinvsqrt_newton = 0, armfasterinvsqrt_newton = 0;
	double cinvsqrt_average = 0, cfastinvsqrt_average = 0, armfastinvsqrt_average = 0, armfasterinvsqrt_average = 0;
	double cinvsqrt_newton_average = 0, cfastinvsqrt_newton_average = 0, armfastinvsqrt_newton_average = 0, armfasterinvsqrt_newton_average = 0;

	//32 mio Testwerte
	count = 32768*1024; // 128mb

	printf("Generiere %d Bytes Testdaten...\n",count*sizeof(float));
	input = malloc(count*sizeof(float));
	output = malloc(count*sizeof(float));

	for(int i = 0;i<count;++i){
		//Wenn RAND_MAX < count ist gibt es eine H�ufung an werten unter 1 => h�herer Fehleranteil
		input[i] =  (float)rand() / (float)i;
	}

	float x = 0.f;
	int iterations = 20;

	printf("\nIterationen %d\n",iterations);
	printf("N = with one Newton-Iteration\n\n");

	//Die Testzeiten der ersten Iteration werden nicht gemessen um die Genauigkeit zu erh�hen
	for(int c = 0; c < iterations+1; ++c){
		printf("=============ITERATION %d=============\n",c);
		if(c == 0) printf("CPU Aufw�rmen...\n");
		printf("\nC InvSqrt: ");//>>>>>>>>>>>>>>>C InvSqrt<<<<<<<<<<<<<<<<<<<<

		gettimeofday(&tv1,NULL);//Startzeitpunkt speichern
		for(int i = 0;i<count;++i){
			x = InvSqrt(input[i]);//Referenzwerte speichern
		}
		gettimeofday(&tv2,NULL);//Endzeitpunkt speichern
		cinvsqrt = (tv2.tv_sec-tv1.tv_sec)*1000000.0 + (tv2.tv_usec-tv1.tv_usec);//Verstrichene Zeit bestimmen
		cinvsqrt_average += c > 0 ? cinvsqrt : 0; //Zu Durchschnitt addieren
		printf("\t\t\t%.1f usec\n", cinvsqrt);

		printf("C FastInvSqrt: "); //>>>>>>>>>>>>>>>C FastInvSqrt with Newton<<<<<<<<<<<<<<<<<<<<

		gettimeofday(&tv1,NULL);
		for(int i = 0;i<count;++i){
			x = fastInvSqrt(magic,input[i]);
		}
		gettimeofday(&tv2,NULL);
		cfastinvsqrt = (tv2.tv_sec-tv1.tv_sec)*1000000.0 + (tv2.tv_usec-tv1.tv_usec);
		cfastinvsqrt_average += c > 0 ? cfastinvsqrt : 0;
		printf("\t\t%.1f usec\n", cfastinvsqrt);

		printf("C FastInvSqrtN: "); //>>>>>>>>>>>>>>>C FastInvSqrt with Newton<<<<<<<<<<<<<<<<<<<<

		gettimeofday(&tv1,NULL);
		for(int i = 0;i<count;++i){
			x = fastInvSqrtNewton(newton_magic,input[i]);
		}
		gettimeofday(&tv2,NULL);
		cfastinvsqrt_newton = (tv2.tv_sec-tv1.tv_sec)*1000000.0 + (tv2.tv_usec-tv1.tv_usec);
		cfastinvsqrt_newton_average += c > 0 ? cfastinvsqrt_newton : 0;
		printf("\t%.1f usec\n", cfastinvsqrt_newton);

		printf("ARM FastInvSqrt: "); //>>>>>>>>>>>>>>>ARMFastInvSqrt<<<<<<<<<<<<<<<<<<<<

		gettimeofday(&tv1,NULL);
		_fast_inv_sqrt(magic,input,output,count);
		gettimeofday(&tv2,NULL);
		armfastinvsqrt = (tv2.tv_sec-tv1.tv_sec)*1000000.0 + (tv2.tv_usec-tv1.tv_usec);
		armfastinvsqrt_average += c > 0 ? armfastinvsqrt : 0;
		printf("\t%.1f usec\n", armfastinvsqrt);

		printf("ARM FastInvSqrtN: "); //>>>>>>>>>>>>>>>ARMFastInvSqrt with Newton<<<<<<<<<<<<<<<<<<<<

		gettimeofday(&tv1,NULL);
		_fast_inv_sqrt_newton(newton_magic,input,output,count);
		gettimeofday(&tv2,NULL);
		armfastinvsqrt_newton = (tv2.tv_sec-tv1.tv_sec)*1000000.0 + (tv2.tv_usec-tv1.tv_usec);
		armfastinvsqrt_newton_average += c > 0 ? armfastinvsqrt_newton : 0;
		printf("\t%.1f usec\n", armfastinvsqrt_newton);

		printf("ARM FasterInvSqrt: ");//>>>>>>>>>>>>>>>ARMFasterInvSqrt<<<<<<<<<<<<<<<<<<<<

		gettimeofday(&tv1,NULL);
		_faster_inv_sqrt(input,output,count);
		gettimeofday(&tv2,NULL);
		armfasterinvsqrt = (tv2.tv_sec-tv1.tv_sec)*1000000.0 + (tv2.tv_usec-tv1.tv_usec);
		armfasterinvsqrt_average += c > 0 ? armfasterinvsqrt  : 0;
		printf("\t%.1f usec\n", armfasterinvsqrt);

		printf("ARM FasterInvSqrtN: ");//>>>>>>>>>>>>>>>ARMFasterInvSqrt with Newton<<<<<<<<<<<<<<<<<<<<

		gettimeofday(&tv1,NULL);
		_faster_inv_sqrt_newton(input,output,count);
		gettimeofday(&tv2,NULL);
		armfasterinvsqrt_newton = (tv2.tv_sec-tv1.tv_sec)*1000000.0 + (tv2.tv_usec-tv1.tv_usec);
		armfasterinvsqrt_newton_average += c > 0 ? armfasterinvsqrt_newton  : 0;
		printf("%.1f usec\n\n", armfasterinvsqrt_newton);
	}

	//Auswertung der Performanz- und Geschwindigkeitsanalyse

	double from = 0.001, to = 10.0, step = 0.001;
	float steps = (to - from) / step;
	if (steps > count){
		printf("Nicht genug Platz um die Qualit�tsauswertung durchzuf�hren\n");
	}else{
		printf("Abweichung von vrsqrte und vrsqrts zur InvSqrt Funktion:\n");
		double error = 0.0, max_error = 0.0, total_error = 0.0;

		int i = 0;
		for(float f = from; f <= to; f += step,++i){
			input[i] = f;
		}

		_faster_inv_sqrt_newton(input,output,i);

		for(int j = 0 ; j < i; ++j){
			total_error += (error = fabs(InvSqrt(input[j]) - output[j]));
			if(error > max_error) max_error = error;
		}

		total_error /= steps;

		printf("Totaler Fehler: %.16f\nMaximaler Fehler: %.16f\n",total_error,max_error);

		printf("\nAbweichung von FastInvSqrtNewton (%X) zur InvSqrt Funktion:\n",newton_magic);

		_fast_inv_sqrt_newton(newton_magic,input,output,(int)steps);

		max_error = total_error = 0.0;
		for(int j = 0 ; j < i; ++j){
			total_error += (error = fabs(InvSqrt(input[j]) - output[j]));
			if(error > max_error) max_error = error;
		}

		total_error /= steps;

		printf("Totaler Fehler: %.16f\nMaximaler Fehler: %.16f\n",total_error,max_error);

		printf("Abweichung von FastInvSqrtNewton (%X) zur InvSqrt Funktion:\n",0x5f3759df);

		_fast_inv_sqrt_newton(0x5f3759df,input,output,(int)steps);

		max_error = total_error = 0.0;
		for(int j = 0 ; j < i; ++j){
			total_error += (error = fabs(InvSqrt(input[j]) - output[j]));
			if(error > max_error) max_error = error;
		}

		total_error /= steps;

		printf("Totaler Fehler: %.16f\nMaximaler Fehler: %.16f\n",total_error,max_error);
	}

	printf("\nDurchschnittswerte f�r %d Iterationen: \n\n",iterations);
	printf("C InvSqrt ");
	printf("Zeitspanne:\t\t%.1f usec\n", cinvsqrt_average/=(double)iterations);
	printf("C FastInvSqrt ");
	printf("Zeitspanne:\t\t\t%.1f usec\n", cfastinvsqrt_average/=(double)iterations);
	printf("C FastInvSqrt Newton ");
	printf("Zeitspanne:\t%.1f usec\n", cfastinvsqrt_newton_average/=(double)iterations);
	printf("ARM FastInvSqrt ");
	printf("Zeitspanne:\t\t\t%.1f usec\n", armfastinvsqrt_average/=(double)iterations);
	printf("ARM FastInvSqrt Newton ");
	printf("Zeitspanne:\t%.1f usec\n", armfastinvsqrt_newton_average/=(double)iterations);
	printf("ARM FasterInvSqrt ");
	printf("Zeitspanne:\t\t%.1f usec\n", armfasterinvsqrt_average/=(double)iterations);
	printf("ARM FasterInvSqrt Newton ");
	printf("Zeitspanne: %.1f usec\n", armfasterinvsqrt_newton_average/=(double)iterations);

	printf("\nGeschwindigkeitsverh�ltnisse:\n\n");
	printf("C InvSqrt/ C FastInvSqrt:\t\t\t\t%.8f\n",cinvsqrt_average/cfastinvsqrt_average);
	printf("ARM FastInvSqrt / ARM FasterInvSqrt:\t%.8f\n",armfastinvsqrt_average/armfasterinvsqrt_average);

	printf("C InvSqrt / ARM FastInvSqrt:\t\t\t%.8f\n",cinvsqrt_average/armfastinvsqrt_average);
	printf("C InvSqrt / ARM FastInvSqrtN:\t\t\t%.8f\n",cinvsqrt_average/armfastinvsqrt_newton_average);
	printf("C InvSqrt / ARM FasterInvSqrt:\t\t\t%.8f\n",cinvsqrt_average/armfasterinvsqrt_average);
	printf("C InvSqrt / ARM FasterInvSqrtN:\t\t\t%.8f\n",cinvsqrt_average/armfasterinvsqrt_newton_average);

	printf("C FastInvSqrt / ARM FastInvSqrt:\t\t%.8f\n",cfastinvsqrt_average/armfastinvsqrt_average);
	printf("C FastInvSqrt / ARM FasterInvSqrt:\t\t%.8f\n",cfastinvsqrt_average/armfasterinvsqrt_average);

	printf("C FastInvSqrt / C FastInvSqrt-Newton:\t%.8f\n",cfastinvsqrt_average/cfastinvsqrt_newton_average);
	printf("ARM FastInvSqrt / ARM FastInvSqrt-Newton:\t\t%.8f\n",armfastinvsqrt_average/armfastinvsqrt_newton_average);
	printf("ARM FasterInvSqrt / ARM FasterInvSqrt-Newton:\t%.8f\n",armfasterinvsqrt_average/armfasterinvsqrt_newton_average);
	printf("ARM FastInvSqrt / ARM FasterInvSqrt-Newton:\t\t%.8f\n",armfastinvsqrt_newton_average/armfasterinvsqrt_newton_average);

	printf("\nDatendurchsatz in MB/s:\n");count *= sizeof(float); count /= 1048576;
	printf("C InvSqrt:\t\t%.4f\n",(double)(count) / (cinvsqrt_average/1000000.0));
	printf("C FastInvSqrt:\t%.4f\n",(double)(count) / (cfastinvsqrt_average/1000000.0));
	printf("C FastInvSqrtN:\t%.4f\n",(double)(count) / (cfastinvsqrt_newton_average/1000000.0));
	printf("ARM FastInvSqrt:\t%.4f\n",(double)(count) / (armfastinvsqrt_average/1000000.0));
	printf("ARM FastInvSqrtN:\t%.4f\n",(double)(count) / (armfastinvsqrt_newton_average/1000000.0));
	printf("ARM FasterInvSqrt:\t%.4f\n",(double)(count) / (armfasterinvsqrt_average/1000000.0));
	printf("ARM FasterInvSqrtN:\t%.4f\n",(double)(count) / (armfasterinvsqrt_newton_average/1000000.0));

	//Speicher f�r Testdaten freigeben
	free(input);free(output);
	return 0;
}
