//Copyright(c) 2013
//Authors: Fabian Wahlster, Steffen Wiewel, Magdalena Papagianni
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com
//License: Read LICENSE.txt located in the root directory

#include <cmath>
#include <stdlib.h>
#include <float.h>
#include <stdio.h>

//#define USE_NEXTAFTER
//Dieses Programm kompiliert nur auf Windows mit der aktuellen C-Runtime 2013 (Visual Studio 2013) da erst seit dieser Version die fehlende
//C99 Funktion nextafterf(float,float) implementiert ist. Alternativ können Sie auch auf Linux mit einer aktuellen gcc Version komplieren
//oder USE_NEXTAFTER undefiniert lassen.

#if defined(_WIN32) || defined(_WIN64) //Windows
    #include <Windows.h>
#else
    #include <string.h>
    #define INT64 int64_t
    #define UINT64 u_int64_t
#endif

void pause(){
    puts("Press 'ENTER' to continue...");
    while (getchar() != '\n');
}

//Interpretiert Daten von Type from zu Type to
template <typename from, typename to> inline to convert(from x){ return (*((to*)&x)); }

template <typename ti, typename tf> inline tf FastInvSqrt(ti m, tf x){
	m = m - (convert<tf,ti>(x) >> 1);
	return convert<ti,tf>(m);
}

template <typename ti, typename tf> inline tf FastInvSqrtNewton(ti m, tf x){
	m = m - (convert<tf, ti>(x) >> 1);
	tf xt = convert<ti, tf>(m);
	xt = xt *(tf(1.5) - (x*0.5)*xt*xt);
	return xt;
}

inline float InvSqrt(float x){
	return 1.f / sqrtf(x);
}
inline double InvSqrt(double x){
	return 1.0 / sqrt(x);
}

template <typename t1> inline t1 GetMagicX(double sigma, int ebits, int mbits){
	int B = pow(2,ebits-1)-1;
	int L = pow(2,mbits);
	return (t1)(1.5*((double)B - sigma)*L);
}

inline int GetMagic32(double sigma){
    //B = Exponent
    //L = Mantissa
	//3 / 2 * (B - sigma)* L
	//B = 2 ^ (8 - 1) - 1 = 127
	//L = 2 ^ 23 = 8388608.0
	return (int)(1.5*(127.0 - sigma)*8388608.0);
}
inline INT64 GetMagic64(double sigma){
	//3 / 2 * (B - sigma)* L
	//B = 2 ^ (11 - 1) - 1 = 1023
	//L = 2 ^ 52 = 4503599627370496
	return (INT64)(1.5*(1023.0 - sigma)*4503599627370496.0);
}

inline double GetSigma(int magic){
    //Analog zu 3 / 2 * (B - sigma)* L ergibt sich für ein gesuchtes Sigma die Formel:
    //B-(magic/(L*1.5))
    return 127.0-((double)magic/(8388608.0*1.5));
}

inline double GetSigma(INT64 magic){
    //Analog zu 3 / 2 * (B - sigma)* L ergibt sich für ein gesuchtes Sigma die Formel:
    //B-(magic/(L*1.5))
    return 1023.0-((double)magic/(4503599627370496.0*1.5));
}

//Templated error function, t1 integral type, t2 float type
template <typename t> inline t AbsError(t exact, t approx){
	//funktioniert nur fuer primitive Typen => fabs und andere standard C Funktionen müssen für Typen mit abweichender Anzahl exponent/mantisse-bits selbst implementiert werden
	return fabs(exact - approx); 
}

//Spezialisiere Template auf float um fabsf anstatt fabs benutzen zu können. 
template <> inline float AbsError(float exact, float approx){
	return fabsf(exact - approx);
}

template <typename t> inline t RelError(t exact, t approx){
	return AbsError(exact,approx)*sqrt(exact);
}

template <> inline float RelError(float exact, float approx){
	return fabsf(exact-approx)*sqrtf(exact);
}

//Suche in der Umgebung i um die initiale MagicValue nach besseren Treffern
template <typename t1, typename t2> t1 SearchMinTotalError(t1 magic, t2 from, t2 to, t2 step, t1 mstep, int no_improvement_max, t2 approx(t1, t2), t2 error(t2, t2)){
	t2 pos_error = 0, neg_error = 0, min_error = FLT_MAX;
	t1 new_magic = magic; t2 steps = (to - from) / step; t2 exact = 0;
    int no_improvement = 0; t1 i = 0;
    //Suche solange neue MagicValue bis sich seit no_improvement_max Versuchen keine Verbesserung einstellt
    while (no_improvement < no_improvement_max){
        //Bestimme den totalen Fehler in dem Bereich von 'from' bis 'to'
		for (t2 f = from; f <= to; f += step){
            exact = InvSqrt(f);//Bestimme akurate inverse Quadratwurzel
            //Bestimme Abweichung zwischen akurater und schneller Methode...
			pos_error += error(exact,approx(magic + i, f));//fabs(sqrt - approx(magic + i, f)); ... erhöhe maximalen Fehler in positiver Richtung
            if (i == 0) continue;//Überspringen wenn i=0 da es keine abweichung gibt
			neg_error += error(exact,approx(magic - i, f));//... erhöhe maximalen Fehler in negativer Richtung
		}
        //Bestimme Mittelwert/Totalen Fehler
		pos_error /= steps;
		neg_error /= steps;

        if (i == 0) {//initialisiere minimalen totalen Fehler
			min_error = pos_error;
		}else{
            //wenn eine neue MagicValue einen geringeren totalen Fehler hat...
			if (neg_error < min_error || pos_error < min_error){
				if (neg_error < pos_error){
                    //... übernehme die bessere MagicValue und setzte den neuen minimalen totalen Fehler
					min_error = neg_error;
					new_magic = magic - i;
				}else{ // if (neg_error >= pos_error)
					//Wenn die MagicValue in positiver Richtung (MagicValue + i) besser ist oder
					//beide MagicValues gleich gut sind (pos_error == neg_error), wählen wir diejenige in positiver Richtung.
					min_error = pos_error;
                    new_magic = magic + i;
				}
                no_improvement = 0;//Es gab eine verbesserung
			}else{
                ++no_improvement;//Es gab keine verbesserung
			}
		}

		printf("Error: %.16f Magic: %llX Range: %lld\n", (double)min_error, (INT64)new_magic, (INT64)i);
        
        pos_error = neg_error = 0;//Zurücksetzen der aktuellen Fehler
        i+=mstep;//Testumgebung ausweiten um mstep
	}
    return new_magic;//Beste MagicValue in mstep Schrittweite
}

//Suche in der Umgebung i um die initiale MagicValue nach besseren Treffern
template <typename t1, typename t2> t1 SearchMinMaxError(t1 magic, t2 from, t2 to, t2 step, t1 mstep, int no_improvement_max, t2 approx(t1, t2), t2 error(t2, t2)){
	t2 pos_error = 0, neg_error = 0,pmax = 0,nmax = 0, min_error = FLT_MAX;
    t1 new_magic = magic; t2 exact = 0;
    int no_improvement = 0; t1 i = 0;
    //Siehe Kommentare oben
    //Bestimme den maximalen Fehler in dem Bereich von 'from' bis 'to'
    while (no_improvement < no_improvement_max){
		for (t2 f = from; f <= to; f += step){
			exact = InvSqrt(f);
            //Bestimme Abweichung zwischen akurater und Schneller Methode, setze maximalen Fehler in postiver / negativer Richtung
            pos_error = error(exact,approx(magic + i, f)); if (pos_error > pmax) pmax = pos_error;//FastInvSqrt<t1,t2>(magic + i, f)
            if (i == 0) continue;
			neg_error = error(exact,approx(magic - i, f)); if (neg_error > nmax) nmax = neg_error;//FastInvSqrt<t1,t2>(magic - i, f))
		}
		if (i == 0) {
			min_error = pmax;
		}else{
			if (nmax < min_error || pmax < min_error){
				if (nmax < pmax){
					min_error = nmax;
					new_magic = magic - i;
				}else{ // if (neg_error >= pos_error)
					min_error = pmax;
                    new_magic = magic + i;
				}
				no_improvement = 0;
			}
			else{
				++no_improvement;
			}
		}

		printf("Error: %.16f Magic: %llX Range: %lld\n", (double)min_error, (INT64)new_magic, (INT64)i);

		pmax = nmax = 0;
		i+=mstep;
	}
	return new_magic;
}

int main(int argc,char *argv[]){
	bool minmax = true; bool newton = false; bool rel = false;
	double from = 0.001, to = 10.0, step = 0.001;
	int e = 8, m = 23;
	for (int i = 1; i<argc; ++i){
		if (strcmp(argv[i], "-minmax") == 0){
			minmax = true; //default
		}
		else if (strcmp(argv[i], "-mintotal") == 0){
			minmax = false;
		}
		else if (strcmp(argv[i], "-abs") == 0){
			rel = false; //default
		}
		else if (strcmp(argv[i], "-rel") == 0){
			rel = true;
		}
		else if (strcmp(argv[i], "-newton") == 0){
			newton = true;
		}
		else if (strcmp(argv[i], "-stepmin") == 0){
			step = FLT_EPSILON;
		}
		else if (strcmp(argv[i], "-from") == 0 && i < argc){			
			from = atof(argv[i + 1]);
		}
		else if (strcmp(argv[i], "-to") == 0 && i < argc){
			to = atof(argv[i + 1]);
		}
		else if (strcmp(argv[i], "-step") == 0 && i < argc){
			step = atof(argv[i + 1]);
		}
		else if (strcmp(argv[i], "-e") == 0 && i < argc){
			e = atoi(argv[i + 1]);
		}
		else if (strcmp(argv[i], "-m") == 0 && i < argc){
			m = atoi(argv[i + 1]);
		}
	}

	int steps = 0;
	double total_error = 0.0f, log2 = log(2), max_error = 0.0f, dif = 0.0f, x ,sigma;

    //Im folgenden Teil des Codes bestimmen wir ein initial Sigma mit dem wir eine initial MagicValue
    //zur weiteren Suche nach unserer idealen MagicValue generieren.
#ifdef USE_NEXTAFTER
	//nextafter(double,double) liefert zwischen 1.0 und 2.0 zuviele Werte um in angemessener Zeit ein
    //Start-Sigma zu bestimmen deshalb benutzen wir nextafterf(float,float)
	for (x = 1.0; x < 2.0; x = _nextafter((float)x, 2.0f), ++steps){ 
#else
	for (x = 1.0; x < 2.0; x += FLT_EPSILON , ++steps){ 
#endif
        //Abweichung von log2(1+x) zu x ausrechnen
		dif = (log(x)/log2) - (x-1.0);
        total_error += dif;//Totaler Fehler
        if(dif > max_error) max_error = dif;//Maximaler Fehler
	}
    //Totaler Fehler ergibt sich aus dem Mittelwert aller Fehler:
	total_error /= (double)steps;
	
	int magic32;
	INT64 magic64;

    //Wenn minmax == true wird nach der MagicValue mit dem kleinsten maximalen Fehler gesucht
    //Wenn minmax == false wird nach der MagicValue mit dem kleinsten Totalen Fehler gesucht
    //from und to bilden die Grenzen des Intervals, das evaluiert werden soll, bzw. für das die MagicValue gesucht wird.
	if (minmax){
		sigma = max_error * 0.5;
	}else{
		sigma = total_error;
	}

    //Wandelt floating point Wert in einen Ganzzahl (int) um
	magic32 = GetMagic32(sigma);
	magic64 = GetMagic64(sigma);

    printf("TotalError: %.22f\nMaxError: %.22f\nSigma: %.22f\nSteps: %d\n\n",total_error,max_error,sigma,steps);

	int newmagic32; 
	INT64 newmagic64;	
	INT64 i = 1000;

	if (minmax){		
		newmagic32 = magic32;
		while (i > 0){
			//Suche in der Umgebung i um die initial MagicValue nach besseren Treffern
			newmagic32 = SearchMinMaxError<int, float>(newmagic32, (float)from, (float)to, (float)step, i, 3
						,newton ? FastInvSqrtNewton<int, float> : FastInvSqrt<int,float>
						,rel ? RelError<float> : AbsError<float> );
			i /= 10;//Schränke Suchumgebung weiter ein
		}
		//pause();
		newmagic64 = magic64;
		i = 1000000000000;
		while (i > 0){
			newmagic64 = SearchMinMaxError<INT64, double>(newmagic64, from, to, step, i, 3
						,newton ? FastInvSqrtNewton<INT64, double> : FastInvSqrt<INT64, double>
						,rel ? RelError<double> : AbsError<double> );
			i /= 10;
		}
	}else{
		newmagic32 = magic32;
		while (i > 0){
			newmagic32 = SearchMinTotalError<int, float>(newmagic32, (float)from, (float)to, (float)step, i, 3
						,newton ? FastInvSqrtNewton<int, float> : FastInvSqrt<int, float>
						,rel ? RelError<float> : AbsError<float>);
			i /= 10;
		}
		//pause();
		newmagic64 = magic64;
		i = 1000000000000;
		while (i > 0){
			newmagic64 = SearchMinTotalError<INT64, double>(newmagic64, from, to, step, i, 3
						,newton ? FastInvSqrtNewton<INT64, double> : FastInvSqrt<INT64, double>
						,rel ? RelError<double> : AbsError<double>);
			i /= 10;
		}
	}

	printf("\n\n>>>>>>>>>>Found MagicValues for minimal %s %s Error\n",minmax ? "maximal" : "total", rel ? "relativ" : "absolut");
	printf(">>>>>>>>>>Using FastInvSqrt %s Newton-Iteraton\n\n", newton ? "with" : "without");

    printf("From: %.16f To: %.16f Step: %.16f\n",from,to,step);

    //Originale MagicValue 0x5f3759df aus dem Quake sourcecode
    //Dieses Sigma (origsigma) sollte ähnlich dem Sigma für die originale 64-bit MagicValue sein. Da aber nicht bekannt ist nach welchen Kriterien die originale MagicValue,
    //und damit auch das originale Sigma bestimmt wurde bleibt uns nichts anderes übrig als dieses Sigma zu benutzen um eine Vergleichs 64-bit MagicValue für unsere
    //MinMax-MagicValue zu erzeugen.
    double origsigma = GetSigma(0x5f3759df);
    INT64 origmagic64 = GetMagic64(origsigma);
    printf("OriginalMagic32: 0x5f3759df OriginalSigma %.22f (assumed)\n",origsigma);
    //optimale MinMax MagicValue von Chris Lomont: 0x5f37642f

    printf("OriginalMagic64: %llX (assumed)\n",origmagic64);
    printf("InitialMagic32: %X NewMagic32: %X\n",magic32, newmagic32);
    printf("NewMagic32 Sigma %.22f\n",GetSigma(newmagic32));
    printf("InitialMagic64: %llX NewMagic64: %llX\n", magic64, newmagic64);
    printf("NewMagic64 Sigma %.22f\n\n",GetSigma(newmagic64));

	//Berechne MagicNumber für x Exponent-Bits und y Mantisse-Bits
	INT64 magicx = GetMagicX<INT64>(GetSigma(newmagic64),e,m);
	printf("Magic for %d Exponent-Bits %d Mantisse-Bits: %llX\n",e,m,magicx);

    pause();
	return 0;
}
