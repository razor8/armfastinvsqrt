\documentclass[11pt]{scrartcl}

\newcommand{\theNumber}{300}
\newcommand{\theName}{Fast Inverse Square Root}

\newcommand{\nameAusarbeitung}{Magdalena Papagianni}
\newcommand{\nameImplementierung}{Fabian Wahlster}
\newcommand{\nameVortrag}{Steffen Wiewel}

\newcommand{\actDate}{ \texttt{17.02.2014} }
 

\input{asp_standard_lib}
\usepackage{hyperref}

\begin{document}

\sheetHeader{Projektaufgabe \theNumber : \theName}{\nameAusarbeitung}{\nameImplementierung}{\nameVortrag}

\begin{center}
\actDate
\end{center}

\begin{abstract}
Diese Arbeit setzt sich mit der {\it Fast Inverse Square Root}, einem berühmten Algorithmus aus der Computergrafik, auseinander. Die Aufarbeitung des Themenkomplexes beginnt mit einer kurzen Einleitung, in der erklärt wird warum dieser Algorithmus in der Vergangenheit sehr wichtig für die Computergrafik war.\\
Daraufhin wird die Problemstellung der Aufgabe analysiert und eine Spezifikation des Themenbereichs vorgenommen.\\
Diese Spezifikation wird im nächsten Teil der Arbeit bearbeitet und verständlich dargestellt. So wird zum Beispiel der mathematische Hintergrund und die Herleitung genauer beleuchtet.\\ 
Im nächsten Teil werden mögliche Lösungsalternativen gegenübergestellt und anschließend verglichen.
Auf Basis des Vergleichs wird die beste Vorgehensweise ausgewählt und in der Dokumentation der Implementierung genauer behandelt. 
Dieser Abschnitt befasst sich mit der Umsetzung des ausgewählten Lösungsansatzes und beschreibt zudem den angegebenen Quellcode in Form einer Benutzer- und Entwicklerdokumentation.\\
Im letzten Kapitel werden alle Ergebnisse zu einem Fazit zusammengefasst.
\end{abstract}
\newpage 

\section{Problemstellung und Spezifikation}
\label{Problemstellung und Spezifikation}

Der {\it Fast Inverse Square Root} Algorithmus liefert, im Vergleich zur normalen mathematischen Berechnung der {\it Inversen Wurzel}, äußerst schnell eine Lösung für die Formel:
\begin{displaymath}
y=\frac{1}{\sqrt{x}}\\
\end{displaymath}
Der {\it Fast Inverse Square Root} Algorithmus wird dabei durch folgende Formel beschrieben 
\begin{displaymath}
y=\frac{1}{\sqrt{x}}\approx MagicNumber-(x\gg 1)\\
\end{displaymath}
und ist eine sehr genaue Annäherung an den exakten Wert der {\it Inversen Wurzel}.\\
\\
\begin{floatingfigure}[r]{4.5cm}
	\includegraphics[width=4cm]{img/surface_normal.png}
	\caption[Normalenfeld von \url{http://upload.wikimedia.org/wikipedia/commons/f/f6/Surface_normal.png}]{Normalenfeld}
	\label{Normalenfeld}
\end{floatingfigure}
Das resultierende {\it y} ist für die Vektormathematik, die hinter der Grafikprogrammierung steht, unabdingbar, da es zum Normalisieren von Vektoren benötigt wird.
Deshalb ist im Bereich der Computergrafik eine performante Lösung dieser Formel, für ein beliebiges {\it x}, sehr wichtig.\\
Einer der ersten Auftritte des Algorithmus war 1999 im Computerspiel {\it Quake III Arena}, ein 3D-Spiel, welches unter anderem auf Lichteffekte setzte. In 3D-Spielen werden normalisierte Vektoren zumeist für die Lichtberechnung herangezogen, um einen möglichst realistischen Lichtreflektion zu simulieren. Auf Abbildung \ref{Normalenfeld} sind die Normalen der Oberfläche zu sehen mit denen der ausgehende Reflektionsvektor bestimmt werden kann.\\
In den meisten Spielen, die vor dem Jahr 2000 erschienen sind, wurde die Grafik ebenso wie die Spielmechanik über den Prozessor berechnet, da in den 90-er Jahren Grafikkarten und {\it Floating-point Units} ({\it FPUs}), wie wir sie heute kennen, noch in ihren Anfangsphasen waren. \\
Somit war die Nutzung sehr performanter Formeln, die gute Annäherungen für komplexere Funktionen lieferten, ein {\it Muss} für die Programmierung von Echtzeitandwendungen.\\
Da minimale Abweichungen von den exakten Werten der Funktionen dem menschlichen Auge in der 3D-Darstellung nicht weiter auffallen, konnte die Illusion der perfekten Beleuchtung durch Annäherungsfunktionen ohne Probleme geschaffen werden.\\
Im Kapitel \ref{Performanz- und Genauigkeitsanalyse} wird im Detail erklärt, wie genau die Ergebnisse der {\it Fast Inverse Square Root} tatsächlich an die echten Werte der {\it Inversen Wurzel} heranreichen.\\ \\
Für die Umsetzung des Algorithmus in {\it ARM}-Assembly wurde ein \board in Kombination mit der auf \eclipse basierenden IDE \ds verwendet.\\
\ds wird von {\it ARM} entwickelt und ist ein Plugin für \eclipse, das Hilfestellungen bei der Entwicklung von {\it ARM}-Assembly bietet. So hat es zum Beispiel eine integrierte Hilfeseite, durch die man mittels eines Tastendrucks \texttt{(F3)} sofort die genaue Definition des ausgewählten Assembler-Befehls und viele zugehörige Informationen angezeigt bekommt.\\
Zudem ist auch ein {\it Debugger} integriert, der es ermöglicht den Quellcode Schritt für Schritt nachzuvollziehen und alle im Programmablauf benötigten Variablen und Register in einer Liste darzustellen.\\
Eine solche Darstellung ist besonders hilfreich, wenn auf sehr vielen Daten gleichzeitig gearbeitet wird. Dies ist dann der Fall, wenn man die Möglichkeiten zur parallelen Datenbearbeitung des {\it Neon-Bausteins}, der Bestandteil der {\it Cortex-A8} CPU ist, nutzt.\\
Weiterhin besitzt das \board Ein- und Ausgabemöglichkeiten über {\it USB 2.0} oder {\it Ethernet} und einen {\it Digitalen Videoausgang}~\footnote{\url{http://beagleboard.org/Products/BeagleBoard-xM}}.\\
Bei dem Arbeitsspeicher handelt es sich um  $512$ MB DDR2. Es sollte angemerkt werden, dass das Hardwaredesign\footnote{\url{http://elinux.org/Beagleboard:BeagleBoard-xM\#Hardware_Design_Files}} frei zugänglich ist.

Im Rahmen dieser Arbeit wurde die Standardausführung verwendet.

\newpage
\section{Mathematische Herleitung}
\label{Mathematische Herleitung}
Die in diesem Kapitel behandelte mathematische Herleitung des Algorithmus, sowie die Bestimmung der {\it Magic-Number}, basiert auf dem Ansatz von {\it Charles McEniry}~\cite{MathBehindFastInverseSqrt}.\\
\ \\
Für die mathematische Herleitung des {\it Fast Inverse Square Root} Algorithmus, ausgehend von der {\it Inversen Wurzel}, ist es zunächst wichtig die {\it IEEE Norm 754}~\footnote{\url{http://de.wikipedia.org/wiki/IEEE_754}} zu verstehen. Diese beschreibt die Darstellung einer {\it Gleitkommazahl} in einem Computersystem und ist wie folgt für 32-Bit definiert:\\
\\

\begin{center}
	\begin{tabular}{|c|c|c|}
	\hline 
	Sign ({\it S}) & Exponent ({\it E}) & Mantisse ({\it M}) \\ 
	\hline 
	1 Bit & 8 Bits & 23 Bits \\ 
	\hline 
	Bit 31 & 30 $\leftarrow$ Bits $\rightarrow$ 23 & 22 $\leftarrow$ Bits $\rightarrow$ 0 \\ 
	\hline 
	\end{tabular} 
\end{center}

\ \\

Bei {\it Gleitkommazahlen} gilt folgendes Format:\\
\begin{displaymath}
x=(-1)^{S}(1,m)\cdot 2^{(E\text{-B)}}=(-1)^{S}\cdot\left(1+\underset{i=1}{\overset{23}{\sum}}(Bit_{23-i}\cdot2^{-i})\right)\cdot2^{(E-B)}
\end{displaymath}
{\it S} ist hierbei das {\it Vorzeichenbit} und gibt an ob die {\it Gleitkommazahl} positiv oder negativ ist. Bei einer Belegung mit $1$ ist die Zahl negativ, bei $0$ positiv.\\
{\it E} beschreibt eine $8$ Bit große positive Binärzahl mit dem Maximalwert $255$, welche in Kombination mit einem {\it Biaswert B} alle möglichen Exponenten der {\it Gleitkommazahl} darstellt. {\it B} ist bei 32-Bit Floats mit $127$ belegt.\\
Die {\it Mantisse M} wird in der Formel dafür genutzt eine Zahl im Bereich von [1,2) zu erzeugen ($ 1,m=1+M/2^{23} $).\\
\ \\
Die entsprechende 32-Bit Darstellung eines {\it Ganzzahlwertes} (Integer) ist wie folgt:
\begin{displaymath}
I=S\cdot2^{31}+E\cdot2^{23}+M
\end{displaymath}
\\
Alternativ gilt für beliebig viele Bits die n-Bit Darstellung:\\
\\
\begin{center}
	\begin{tabular}{|c|c|c|}
	\hline 
	Sign ({\it S}) & Exponent ({\it E}) & Mantisse ({\it M}) \\ 
	\hline 
	1 Bit & b-Bits & (n-1-b) Bits \\ 
	\hline 
	\multicolumn{3}{|c|}{n-Bits} \\
	\hline 
	\end{tabular} 
\end{center}

\ \\
Bei dem bearbeiteten Algorithmus ist $x>0$, für $y=\frac{1}{\sqrt{x}}$. Daraus folgt $y\geq0$ und $S=0$.
Dann gilt: \\
\begin{equation}
x=(i+m_{x})\cdot2^{e_{x}}
\label{Gleitkommadarstellung}
\end{equation}

Die allgemeine Repräsentation von Fließkommazahlen hängt deshalb nur von der Mantisse und der Anzahl der Bits zur Darstellung des Exponenten ab. \\
\ \\
Die entsprechende Integer Darstellung für n-Bit Floats ist wie folgt:\\
\begin{equation}
I_{x}=E_{x}L+M_{x}
\label{IntegerDarstellung}
\end{equation}

Bei einer n-Bit langen Darstellung ergibt sich für $L=2^{n-1-b}$ und $B=2^{b-1}-1$. Der Wert des Exponenten $e_{x}$ entspricht $E_{x}-B$. Dadurch können Werte von $-127$ bis $128$ dargestellt werden.\\

\begin{equation}
e_{x}=E_{x}-B
\label{Exponent}
\end{equation}

\begin{displaymath}
\textrm{mit}\ L=2^{n-1-b}\ \textrm{und}\ B=2^{b-1}-1\\
\end{displaymath}

Zudem definieren wir:

\begin{equation}
m_{x}=\frac{M_{x}}{L}
\label{Mantisse}
\end{equation}
\ \\

Im Folgenden wird aus der {\it Inversen Wurzel} der {\it Fast Inverse Square Root} Algorithmus hergeleitet.
\begin{align*}
y&=\frac{1}{\sqrt{x}}\\
\Leftrightarrow y&=x^{-\frac{1}{2}}\\
\Leftrightarrow\log_{2}y&=\log_{2}x^{-\frac{\text{1}}{2}}\\
\Leftrightarrow\log_{2}y&=-\frac{\text{1}}{2}\log_{2}x
\end{align*}
\ \\
Wir ersetzen {\it x} und {\it y} durch die Gleitkommadarstellung aus Formel~(\ref{Gleitkommadarstellung}).

\begin{align*}
\log_{2}[(1+m_{y})\cdot2^{e_{y}}]&=-\frac{1}{2}\log_{2}[(1+m_{x})\cdot2^{e_{x}}]\\
\overset{Logarithmusgesetze}{\Leftrightarrow}\log_{2}(1+m_{y})+\log_{2}\cdot2^{e_{y}}&=-\frac{1}{2}log_{2}(1+m_{x})-\log_{2}2^{e_{x}}\\
\overset{Logarithmusgesetze}{\Leftrightarrow}\log_{2}(1+m_{y})+e_{y}&=-\frac{1}{2}\log_{2}(1+m_{x})-\frac{1}{2}e_{x}\\
\end{align*}

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=1\textwidth]{img/logx_sigma_2.png}
    \caption[Graph von $\log_{2}(x+1)$, Ursprungsgerade und Fehlerfunktion] {Graph von \textcolor{blue}{$\log_{2}(x+1)$}, Ursprungsgerade und \textcolor{red}{Fehlerfunktion} \ref{Fehlerfunktion} }
    \label{sigma}
  \end{center}
\end{figure}

In Abbildung~\ref{sigma} ist erkennbar, dass für Werte von $x\leq1$ der $\log_{2}(x+1)$ in etwa der Ursprungsgerade entspricht ($\log_{2}(x+1)\approx x$). Um diesen Fehler auszugleichen, wählen wir $\sigma$ (Sigma) so, dass gilt:
\ \\
 \begin{equation}
\log_{2}(x+1) \approxeq x+\sigma
\label{log}
\end{equation} 
Unsere Gleichung vereinfacht sich auf eine lineare Näherung, die für Computer einfacher zu verarbeiten ist.
\begin{align*}
\overset{(\ref{log})}{ \Leftrightarrow} m_{y}+\sigma+e_{y}&=-\frac{1}{2}m_{x}-\frac{1}{2}\sigma-\frac{1}{2}e_{x}\\
\overset{(\ref{Mantisse})}{\Leftrightarrow} \frac{M_{y}}{L}+\sigma+e_{y}&=-\frac{1}{2}\frac{M_{x}}{L}-\frac{1}{2}\sigma-\frac{1}{2}e_{x}\\
\Leftrightarrow M_{y}+L\cdot\sigma+L\cdot e_{y}&=-\frac{1}{2}M_{x}-\frac{1}{2}(\sigma\cdot L)-\frac{1}{2}e_{x}\cdot L\\
\overset{(\ref{Exponent})}{\Leftrightarrow} M_{y}+L\cdot(E_{y}-B)&=-\frac{1}{2}M_{x}-\frac{3}{2}(\sigma\text{\ensuremath{\cdot}L)}-\frac{1}{2}(E_{x}-B)\cdot L\\
\Leftrightarrow M_{y}+E_{y}\cdot L&=B\cdot L-\frac{1}{2}M_{x}-\frac{3}{2}(\sigma\cdot L)-\frac{\text{1}}{2}(E_{x}\cdot L)+\frac{1}{2}(B\cdot L)\\
\Leftrightarrow E_{y}\cdot L+M_{y}&=-\frac{1}{2}[M_{x}+E_{x}\cdot L]-\frac{3}{2}(\sigma\cdot L)+\frac{3}{2}(B\cdot L)\\
\overset{(\ref{IntegerDarstellung})}{\Leftrightarrow} E_{y}\cdot L+M_{y}&=\frac{3}{2}[B-\sigma]\cdot L-\frac{1}{2}[M_{x}+E_{x}\cdot L]\\
\Leftrightarrow I_{y}&=\frac{3}{2}[B-\sigma]-\frac{1}{2}I_{x}\\
\end{align*}

$\Rightarrow  I_{y}=R-\frac{1}{2}I_{x}$ \quad mit $R=\frac{3}{2}[B-\sigma]\cdot L$ als {\it Magic-Number}.\\
\ \\
Durch die obigen Umformungen erkennt man, dass es sich bei $I_{y}=R-\frac{1}{2}I_{x}$ um den {\it Fast Inverse Square Root} Algorithmus handelt. \\
Die Division durch $2$ kann in {\it C} durch einen {\it Bitshift} nach rechts durchgeführt werden.\\
Daraus ergibt sich folgender {\it C-Code}:\\
\lstset{language=C, basicstyle=\small,  numbers=left, numberstyle=\tiny}
\begin{lstlisting}[caption={Teil des {\it Quake III: Arena} Quellcodes~\cite{FastInverseSqrtQuake}},frame=single, captionpos=b, label=code-comm-task, xleftmargin=.03\textwidth]
i = n - ( i >> 1 );
\end{lstlisting}
\ \\
Beispielhaft für 32-Bit Werte kann man die {\it Magic-Number} wie folgt in Abhängigkeit von  $\sigma$ bestimmen:
\begin{displaymath}
R=\frac{3}{2}\cdot L \cdot [B-\sigma] = \frac{3}{2}\cdot 2^{23} \cdot 
(127-0.03911765416462742445068)=\texttt{0x5F387D4A}
\end{displaymath}
Dasselbe kann analog für 64- und 128-Bit Fließkommazahlen berechnet werden. Dazu müssen lediglich die Konstanten {\it L} und {\it B} angepasst werden.

\newpage
\section{Dokumentation der Implementierung}
\label{Dokumentation der Implementierung}
\subsection{Finden der optimalen Magic-Number}
\label{Finden der optimalen Magic-Number}
Dieses Kapitel behandelt den Quellcode der Datei {\it sigma.cpp}, welche im Ordner {\it sigma} zu finden ist. Anweisungen zum Kompilieren und Ausführen des Programms sind als Kommentare in der Quellcodedatei zu finden.\\
\ \\
Es gibt mehrere Herangehensweisen um eine {\it Magic-Number}, die bestimmte Kriterien erfüllt, zu bestimmen. Da 
die {\it Magic-Number} direkt von $\sigma$ abhängt, ist es wichtig im Vorfeld eine gute Wahl für dieses $\sigma$ 
zu treffen.\\
Das erste Kriterium ist, $\sigma$ so zu wählen, dass der totale Fehler von $\log_{2}(x+1) \approx x$ 
minimal wird, also $\underset{0}{\overset{1}{\int}}((\log_{2}(x+1)-(x-\sigma))=0$.\\
\ \\
In unserem Algorithmus lösen wir das Problem wie folgt: Alle {\it Funktionswerte} von $f(x)=\left| \log_{2}(x+1)-x 
\right|$ für $0 < x < 1$ werden aufaddiert und durch die finite Anzahl der {\it Eingabewerte} geteilt. Dieses $\sigma
$ eignet sich gut als {\it Startwert} zur Suche nach einer {\it Magic-Number}, die den totalen Fehler minimiert. \\
\ \\
Das zweite Kriterium nach dem man $\sigma$ bestimmen kann, ist den maximalen Fehler zu minimieren: 
$\left| (x+\textrm{max})-(x+ \sigma)\right|=\left|(x-(x+\sigma))\right| \Rightarrow \sigma = \frac{\textrm{max}}
{2}$ .\\
\ \\
Dazu evaluieren wir wieder alle {\it Funktionswerte} $f(x)$ und merken uns dasjenige $f(x_{n})$, welches größer als 
alle vorherigen Funktionswerte ausgehend von  $f(x_{0})$ ist, sodass wir schließlich den maximalen Fehler erhalten 
(max = $f(x_{n})$). Aus diesem kann dann unser $\sigma$ bestimmt werden. \\
\ \\
\label{Fehlerfunktion}
Wenn man den Graphen (siehe Abb.~\ref{Ideale Magic ohne Newton}) der {\it Fehlerfunktion} $E(x)=\left| \frac{1}{\sqrt{x}}-FastInvSqrt(x)\right|$ genauer 
betrachtet, stellt man fest, dass im Bereich $0 < x < 10$ der größte {\it Fehleranteil} liegt.
Um in diesem Bereich den Fehler bestmöglich zu kompensieren, haben wir uns dazu entschieden, $\sigma$ nach dem 
zweiten Kriterium zu wählen.\\
\ \\
Im Bereich von $10 < x <$ {\it FLT\_MAX} (höchster darstellbarer Wert einer 32-Bit Float-Variable) ist auch der totale Fehler mit unserer {\it MinMax-Magic-Number} 
vertretbar gering. Mit Hilfe der oben genannten Formel errechnen wir eine initiale {\it Magic-Number}, in deren 
näheren Umgebung unsere gesuchte ideale Konstante liegt.
Um diese ideale {\it Magic-Number} zu finden haben wir einen {\it BruteForce} Algorithmus entwickelt, der den 
maximalen (nach Wahl auch den totalen) Fehler in einem festgelegten Intervall analysiert und basierend darauf schrittweise die 
Suchumgebung einschränkt.\\
\ \\
Aus dem oben genannten Grund beschränken wir unser Intervall auf den Bereich zwischen $0.001$ bis $10.0$ und 
wählen eine {\it Schrittweite} von $0.001$ um ca. $10000$ {\it Eingabewerte} zu evaluieren.
Die Wahl der {\it unteren Intervallgrenze} $0.001$ folgt aus dem starken Anstieg des Fehlers in Richtung $x 
\rightarrow 0$ und stellt einen vertretbaren {\it Minimalwert} für mögliche Eingaben des {\it FastInvSqrt}-Algorithmus dar.\\
Ausschlaggebend für die Geschwindigkeit unseres Verfahrens ist die Schrittweite {\it mstep}, die bei jeder Iteration 
auf die aktuelle initiale {\it Magic-Number} aufaddiert bzw. subtrahiert wird.
Für diese neue {\it Magic-Number} wird dann der maximale Fehler in dem festgelegten Intervall bestimmt: ist er 
geringer, merken wir uns diese {\it Magic-Number} und wiederholen diesen Vorgang solange bis sich keine 
Verbesserung mehr einstellt und geben die gefundene Konstante zurück.\\
\ \\
Ausgehend von dieser Konstante starten wir das Verfahren erneut, setzen aber $ \textrm{mstep}=
\frac{\textrm{mstep}}{10}$ um den Suchbereich weiter einzuschränken. 
Der Algorithmus terminiert wenn {\it mstep = 1} ist und keine bessere {\it Magic-Number} gefunden werden kann.
Für die Suche nach der 32-Bit {\it Magic-Number} hat sich für {\it mstep} $1000$ und für die 64-Bit Variante
$1000000000000$ als guter {\it Startwert} für einen schnellen Ablauf des Suchalgorithmus herausgestellt.\\

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{img/ideal_magic.png}
	\caption[Graph der idealen Magic-Number im Bereich von 0.1 bis 10 mit Newton-Iteration] {Graph der idealen {\it Magic-Number} im Bereich von [0.1,10] mit {\it Newton-Iteration} }
    \label{Ideale Magic}
  \end{center}
\end{figure}

Abbildung \ref{Ideale Magic} zeigt den Graphen der Funktion $f(x)=\frac{1}{\sqrt{x}}$ in schwarz und die {\it Fast 
Inverse Square Root} Funktion mit einer {\it Newton-Iteration} in blau. Die Fehlerfunktion, hier in rot, wird aus 
Gründen der Sichtbarkeit um den Faktor $10$ vergrößert. Aus selbigem Grund ist die Abbildung auf den Bereich von 
$0.1$ bis $10$ beschränkt, da die Funktionswerte in Richtung $x\rightarrow0$ nicht mehr angemessen darstellbar sind.\\

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{img/ideal_magic_no_newton.png}
	\caption[Graph der idealen Magic-Number im Bereich von 0.1 bis 10 ohne Newton-Iteration] {Graph der idealen {\it Magic-Number} im Bereich von [0.1,10] ohne {\it Newton-Iteration} }
    \label{Ideale Magic ohne Newton}
  \end{center}
\end{figure}

Die Farbbelegung in Abbildung \ref{Ideale Magic ohne Newton} ist identisch zu der in Abbildung \ref{Ideale Magic}.
Der große Genauigkeitsgewinn durch die {\it Newton-Iteration} wird beim Vergleich der beiden Abbildungen deutlich sichtbar. Der Geschwindigkeitsunterschied wird im Kapitel \ref{Performanz- und Genauigkeitsanalyse} näher erläutert.\\

Unsere optimalen {\it Magic-Numbers} für den Bereich von 0.001 und 10 lauten: \\
\begin{center}
\begin{tabular}{|l|l|l|}
\hline 
MinMax & 32-Bit & 64-Bit \\ 
\hline 
FastInvSqrt & \texttt{0x5F3970D6} & \texttt{5FE72E1AE036484D} \\ 
\hline 
FastInvSqrtN & \texttt{0x5F387D4A} & \texttt{5FE70FAA9D7A986A} \\ 
\hline 
\end{tabular}
\end{center}
\ \\
Über die {\it Kommandozeilenparameter –mintotal} und {\it –minmax} kann festgelegt werden mit welchem 
Kriterium (minimaler totaler Fehler oder minimaler maximaler Fehler) nach der {\it Magic-Number} gesucht werden 
soll. {\it –newton} legt fest, ob der {\it FastInvSqrt} Algorithmus nach der Näherung noch eine Iteration des {\it 
Newtonverfahrens} ausführt. Die Parameter {\it –from} und {\it –to} spezifizieren die {\it Grenzen} des {\it 
Testintervalls} und {\it –step} bestimmt die {\it Schrittweite}.
Um unsere gefundene {\it Magic-Number} mit Hilfe unseres Programms {\it SqrtPlotter} mit der originalen 64-Bit 
Konstante vergleichen zu können, bestimmen wir aus der bekannten 32-Bit {\it Magic-Number} \texttt{0x5F3759DF} den 
Wert für $\sigma$ und berechnen mit der obigen Formel die 64-Bit Konstante.
\ \\
\subsection{Fast Inverse Square Root Algorithmus in ARM Assembly}
\label{Fast Inverse Square Root Algorithmus in ARM Assembly}
Unsere {\it ARM}-Assembly Implementierung der unterschiedlichen {\it FastInvSqrt} Algorithmen basiert 
grundsätzlich auf folgender Struktur: wir laden zuerst einen grösstmöglichen Datensatz aus dem {\it Input-Float-Array} vom Arbeitsspeicher in die vorhandenen {\it Quad-Register} der {\it Neon-Einheit} und verarbeiten diesen 
entsprechend unserer unterschiedlichen Implementierungen.\\

Die Ergebniswerte werden dann von den {\it Quad-Register} in das {\it Output-Array} gespeichert. Die Anzahl der 
verarbeiteten Floats subtrahieren wir von der übergebenen {\it count}-Variable, um die verbleibende Anzahl an 
Floatwerten zu erhalten und springen zurück an den Anfang der Schleife. \\

Dort überprüfen wir, ob diese Variable kleiner dem Wert der maximal gleichzeitig abarbeitbaren Datensätze ist und 
springen gegebenenfalls zu dem Programmblock zur Abarbeitung des nächst kleineren Datensatzes.\\In diesem 
Block verfahren wir nach dem gleichen Prinzip bis schließlich der kleinstmögliche Datensatz ($1$ Float) abgearbeitet 
ist.\\

Unsere Implementierung des Algorithmus (auch mit einer {\it Newton-Iteration}) ist im Gegensatz zu den 
vorhandenen {\it vrsqrte} und {\it vrsqrts} Instruktionen auf die Standard-Vektor-Befehle (Grundrechenarten und 
Bit-Shift) limitiert. Zunächst laden wir unsere {\it Magic-Number} vier mal hintereinander in das {\it Quad-Register}
und die Konstanten $0.5$ und $1.5$, zur späteren Verwendung in der {\it Newton-Iteration}, in die Register 
\texttt{d15[0]} bzw. \texttt{d15[1]}.\footnote{In der beispielhaften Implementierung gehen wir davon aus, dass vier Eingabewerte x in \texttt{q1} geladen wurden.} \\
\ \\
Daraufhin bestimmen wir {\it xhalf} mit dem Befehl \texttt{vmul.f32 q4, q1, d15[0]} zur Verwendung in $x(1.5-
\textrm{\it{xhalf}}\cdot x\cdot x)$ und führen dann die eigentliche Approximation aus: \texttt{vshr.i32 q1, q1, 
\#1} shiftet $4$ Floats jeweils um $1$ {\it Bit} nach rechts, was $x>>1$ im original C-Algorithmus entspricht. Dann 
subtrahieren wir das Ergebnis der obigen Operation mit \texttt{vsub.i32 q1, q0, q1} von der {\it Magic-Number}, die in \texttt{q0} gespeichert ist, und speichern das Ergebnis in \texttt{q1}. \\

Jetzt beginnen wir mit der eigentlichen {\it Newton-Iteration} in dem wir drei mal hintereinander \texttt{vmul.f32 
q4, q4, q1} ausführen, was $\textrm{\it{xhalf}}\cdot x\cdot x\cdot x$ entspricht.\\

In der darauf folgenden Instruktion \texttt{vmul.f32 q1, q1, d15[1]} benutzen wir die Konstante $1.5$ um $x\cdot 
1.5$ auszurechnen und schließlich mit \texttt{vsub q1, q1, q4,} $\textrm{\it{xhalf}}\cdot x\cdot x\cdot x$ von 
$1.5\cdot x$ abzuziehen, womit die {\it Newton-Iteration} beendet ist.\\

Der Algorithmus zu Verwendung der vorhandenen Instruktionen führt zuerst\\
 \texttt{vrsqrte.f32 q1, q1} , also den 
{\it Näherungsschritt}, und dann den {\it Newton-Iterationsschritt} \texttt{vrsqrts.f32 q1, q1} aus. \\


\subsection{Performanz- und Genauigkeitsanalyse}
\label{Performanz- und Genauigkeitsanalyse}
Die Analyse der Geschwindigkeit, sowie Genauigkeit bezieht sich auf das Rahmenprogramm in der {\it mycode.c} Quelldatei.\\
\ \\
Zunächst generieren wir $33554432$ zufällige Float-Werte, die uns als Testdaten dienen. Dies entspricht $128$ MB an zu verarbeitenden Daten. Die Performanzmessungen werden in einer Schleife $20$ mal wiederholt, um aussagekräftige Durchschnittswerte zu erhalten. Bei einem zusätzlichen vorangestellten Durchlauf der Schleife werden keine Messwerte gespeichert, da diese bei der ersten Ausführung sehr großen Schwankungen unterliegen.\\
Die Messwerte erheben wir, indem wir vor und nach dem Ausführen der jeweiligen Funktion die aktuelle Zeit auslesen. Die Differenz dieser Zeitpunkte entspricht der, beim Ablauf des Algorithmus, verstrichenen Zeit.
Um die durchschnittliche Laufzeit jedes einzelnen Algorithmus, nach dem Schleifendurchlauf, berechnen zu können, addieren wir die jeweilige Zeitspanne zu den vorherigen Messergebnissen und teilen diese schließlich durch die Anzahl der Messungen.\\
In der nachfolgenden Tabelle entspricht {\it C InvSqrt} dem naiven Ansatz $\frac{1}{\sqrt{x}}$ in C. Bei {\it FastInvSqrt} handelt es sich um unsere Implementierung ohne {\it Newton-Iteration}, dementsprechend steht der Zusatz {\it Newton} für eine Implementierung mit {\it Newton-Iteration}. {\it FasterInvSqrt} beschreibt den Algorithmus unter Verwendung der vorhandenen CPU-Instruktionen.

\begin{center}
\begin{tabular}{|l|c|c|}
\hline 
Algorithmus & \O -Laufzeit (Mikrosekunden) & Datendurchsatz (MB/s) \\ 
\hline 
C InvSqrt & 13735174.6 & 9.3191 \\ 
\hline 
C FastInvSqrt & 4539772.0 & 28.1952 \\ 
\hline 
C FastInvSqrt Newton & 10461820.8 & 12.2350 \\ 
\hline 
ARM FastInvSqrt & 475990.2 & 268.9131 \\ 
\hline 
ARM FastInvSqrt Newton & 491108.7 & 260.6348 \\ 
\hline 
ARM FasterInvSqrt & 472854.6 & 270.6963 \\ 
\hline 
ARM FasterInvSqrt Newton & 426512.3 & 300.1085 \\ 
\hline 
\end{tabular} 
\end{center}
\ \\
Basierend auf den durchschnittlichen Laufzeiten, bestimmen wir die Geschwindigkeitsverhältnisse der Algorithmen zueinander:\\

\resizebox{\linewidth}{!}{%
\begin{tabular}{|l|c|c|c|c|c|}
\hline 
& C InvSqrt & C FastInvSqrt & Arm FastInvSqrt & Arm FastInvSqrtN & Arm FasterInvSqrtN \\ 
\hline 
C InvSqrt & 1 & 3.02552075 & 28.85600584 & 27.96769014 & 32.20346269 \\ 
\hline 
C FastInvSqrt & 0.33052160 & 1 & 9.53753360 & 9.24392502 & 10.64394157 \\ 
\hline 
Arm FastInvSqrt & 0.03465483 & 0.10484892 & 1 & 0.96921557 & 1.15145236 \\ 
\hline 
Arm FastInvSqrtN & 0.03575554 & 0.10817915 & 1.03176220 & 1 & 1.15145260 \\ 
\hline 
Arm FasterInvSqrtN & 0.03105255 & 0.09395015 & 0.89605269 & 0.86846822 & 1 \\
\hline 
\end{tabular} 
}
\ \\
\ \\
\ \\
Zu beachten ist, dass sich das Verhältnis der jeweiligen Algorithmen aus der Laufzeit des Algorithmus in der ersten Spalte, als Dividenden, und der Laufzeit des Algorithmus der ersten Zeile, als Divisor, ergeben.\\
Zur Messung der Genauigkeit gehen wir ähnlich wie in der {\it sigma.cpp} vor, indem wir den maximalen und totalen Fehler in dem selben Testintervall evaluieren.\\
\begin{center}
\begin{tabular}{|l|l|l|}
\hline 
Algorithmus & Totaler Fehler & Maximaler Fehler \\ 
\hline 
vrsqrte und vrsqrts & 1.2217062582956790 & 528.21850585937500 \\ 
\hline 
FastInvSqrtNewton (\texttt{5F387D4A}) & 0.0008160360802029 & 0.0267238616943359 \\ 
\hline 
FastInvSqrtNewton (\texttt{5F3759DF}) & 0.0005927063492682 & 0.0377101898193359 \\ 
\hline 
\end{tabular} 
\end{center} 
\ \\
Der größte Geschwindigkeitssprung liegt zwischen der naiven C-Implementierung und dem bis zu $32$ mal schnelleren 
{\it ARM FasterInvSqrtN} Code. Dies spiegelt sich auch in dem Datendurchsatz wieder: die {\it C  InvSqrt} Funktion 
verarbeitet ca. $9$ MB/s, die {\it ARM} Umsetzung mit \texttt{VRSQRTE} und \texttt{VRSQRTS} hingegen $300$ MB/s.\\
Letzterer Algorithmus ist etwa $15$\% schneller als unsere limitierte {\it ARM} Implementierung ({\it ARM FastInvSqrtN}), dafür ist der maximale Fehler aber ca. $19765$ mal so hoch. Mit unserer optimalen {\it Magic-Number} ist der totale Fehler im Bereich von $0.001$ und $10$ um den Faktor $1497$ kleiner.\\

\newpage
\section{Ergebnisse}
\label{Ergebnisse}
Der Geschwindigkeitsvorteil unseres limitierten {\it ARM}-Assembly Codes zu der naiven C-Implementierung ist mit 
einem bis zum $28$ mal schnelleren Ablauf immens.\\
Die Abstriche, die man bei der Genauigkeit des Algorithmus unter Verwendung der vorhandenen CPU-
Instruktionen machen muss, stehen in keinem Verhältnis zu dem niedrigen Geschwindigkeitszuwachs im 
Vergleich mit unserer Implementierung ({\it Newton-Iteration}).\\
Zwischen unserer optimalen {\it Magic-Number} und \texttt{0x5F3759DF} gibt 
es einen messbaren, aber keinen sichtbaren Genauigkeitsvorteil im Bezug auf dem Maximalen Fehler.\\
Selbst bei der Implementierung ohne {\it Newton-Iteration} gibt es keine sichtbare Abweichung.
Der Totale Fehler liegt weiterhin in einem akzeptablen Bereich, wie auf folgenden Bildern zu sehen ist.

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=1\textwidth]{img/normal_comparison.png}
	\caption[Normalmap mit unseren optimalen Magic-Numbers mit Original von \url{http://www.fotocommunity.de/pc/pc/extra/fav/userid/465268/display/30625403}] {Normalmap mit unseren optimalen {\it Magic-Numbers} 
	\\(Original: \copyright~Arndt Deckers)}
    \label{Normalen Vergleich}
  \end{center}
\end{figure}

Es macht also durchaus Sinn bei performance-kritischen Anwendungen (z.B. Echtzeitgrafik und Simulation) statt einer 
Hochsprachenumsetzung, maschinennahen Assembler zu bevorzugen, auch wenn dies Fachkenntnisse benötigt und 
eventuell längere Entwicklungszeit nach sich zieht.

\newpage
\listoffigures
\bibliographystyle{plain}
\nocite{FastInverseSqrtLomont,FastInverseSqrtRevisited}
\bibliography{FastInvSqrtBib}
\end{document}