//Copyright(c) 2013
//Authors: Fabian Wahlster, Steffen Wiewel, Magdalena Papagianni
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com
//License: Read LICENSE.txt located in the root directory

#define POS(x,y) map[(x)+(y)*width]
#define TPOS(x,y) tmpmap[(x)+(y)*width]
#define NPOS(x,y) normalmap[(x)+(y)*width]
#define VALID(m,n) (((n) >= 0) && ((n) < height) && ((m) >= 0) && ((m) < width))
#define SATURATE(x) (((x) < 0) ? 0 : (((x) > 1) ? 1 : (x)))
#define SMOOTHSTEP(x) ((x)*(x)*(3 - 2*(x)))

#define FOREACHORIGPIXEL for (UINT y=0; y<o_height; ++y){ for (UINT x=0; x<o_width; ++x){
#define FOREACHPIXEL for (UINT y=0; y<height; ++y){ for (UINT x=0; x<width; ++x){
#define END_FOREACHPIXEL } }

inline float Generator::deriveX(int x, int y, float scale){
	float u, v; 
	if(VALID(x+1,y)){
		u = POS(x+1,y);
	}else{
		u = POS(x,y);
	}
	if(VALID(x-1,y)){
		v = POS(x-1,y);
	}else{
		v = POS(x,y);
	}
	return ((u-v)*scale)/2.0f;
}

inline float Generator::deriveY(int x, int y, float scale){
	float u, v; 
	if(VALID(x,y+1)){
		u = POS(x,y+1);
	}else{
		u = POS(x,y);
	}
	if(VALID(x,y-1)){
		v = POS(x,y-1);
	}else{
		v = POS(x,y);
	}
	return ((u-v)*scale)/2.0f;
}

inline float Generator::fastInvSqrt(float x){
	int i = *(int*)&x;
	float xhalf = x * 0.5f;
	//no newton:	0x5F3970D6
	//with newton:	0x5F387D4A
	i = 0x5F3970D6 - (i >> 1);
	x = *(float*)&i;
	//x = (x*(1.5f - xhalf*x*x));
	return x;
}

void Generator::generateNormalMap(float scale){
	float u, v, n, z;
	scale *= (width == height) ? width : (width + height) / 2;
	FOREACHORIGPIXEL
			u = -deriveX(x,y,scale);
			v = -deriveY(x,y,scale);
			n = fastInvSqrt(u*u + v*v + 1.0f);//1.f/sqrt(u*u + v*v + 1.0f);
			u *= n; u = (u+1.0f)/2.0f;
			v *= n; v = (v+1.0f)/2.0f;
			z = (n+1.0f)/2.0f;
			//when using fastInvSqrt we need to clamp ( SATURATE ) the values between 0 and 1
			//because the first estimate might not be enough to normlize the vector
			NPOS(x, y) = Color(SATURATE(u), SATURATE(v), SATURATE(z));
	END_FOREACHPIXEL
}