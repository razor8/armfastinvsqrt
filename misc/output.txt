Keine Optimierung durch den Compiler

Generiere 134217728 Bytes Testdaten...

Iterationen 20
N = with one Newton-Iteration

=============ITERATION 0=============
CPU Aufw�rmen...

C InvSqrt:          13734223.0 usec
C FastInvSqrt:      4543273.0 usec
C FastInvSqrtN:     10464112.0 usec
ARM FastInvSqrt:    1031891.0 usec
ARM FastInvSqrtN:   491303.0 usec
ARM FasterInvSqrt:  472229.0 usec
ARM FasterInvSqrtN: 426178.0 usec

=============ITERATION 1=============

C InvSqrt:          13735138.0 usec
C FastInvSqrt:      4538818.0 usec
C FastInvSqrtN:     10465545.0 usec
ARM FastInvSqrt:    475830.0 usec
ARM FastInvSqrtN:   490692.0 usec
ARM FasterInvSqrt:  472503.0 usec
ARM FasterInvSqrtN: 426087.0 usec

=============ITERATION 2=============

C InvSqrt:          13731323.0 usec
C FastInvSqrt:      4538453.0 usec
C FastInvSqrtN:     10464172.0 usec
ARM FastInvSqrt:    475800.0 usec
ARM FastInvSqrtN:   490905.0 usec
ARM FasterInvSqrt:  473510.0 usec
ARM FasterInvSqrtN: 427918.0 usec

=============ITERATION 3=============

C InvSqrt:          13734436.0 usec
C FastInvSqrt:      4539307.0 usec
C FastInvSqrtN:     10461273.0 usec
ARM FastInvSqrt:    475830.0 usec
ARM FastInvSqrtN:   491119.0 usec
ARM FasterInvSqrt:  477326.0 usec
ARM FasterInvSqrtN: 426178.0 usec

=============ITERATION 4=============

C InvSqrt:          13733063.0 usec
C FastInvSqrt:      4538635.0 usec
C FastInvSqrtN:     10458619.0 usec
ARM FastInvSqrt:    477966.0 usec
ARM FastInvSqrtN:   491729.0 usec
ARM FasterInvSqrt:  472626.0 usec
ARM FasterInvSqrtN: 425843.0 usec

=============ITERATION 5=============

C InvSqrt:          13738862.0 usec
C FastInvSqrt:      4538727.0 usec
C FastInvSqrtN:     10462432.0 usec
ARM FastInvSqrt:    475647.0 usec
ARM FastInvSqrtN:   491181.0 usec
ARM FasterInvSqrt:  472229.0 usec
ARM FasterInvSqrtN: 426178.0 usec

=============ITERATION 6=============

C InvSqrt:          13737824.0 usec
C FastInvSqrt:      4539978.0 usec
C FastInvSqrtN:     10458771.0 usec
ARM FastInvSqrt:    475891.0 usec
ARM FastInvSqrtN:   490967.0 usec
ARM FasterInvSqrt:  472381.0 usec
ARM FasterInvSqrtN: 425934.0 usec

=============ITERATION 7=============

C InvSqrt:          13736602.0 usec
C FastInvSqrt:      4540832.0 usec
C FastInvSqrtN:     10461151.0 usec
ARM FastInvSqrt:    475982.0 usec
ARM FastInvSqrtN:   490845.0 usec
ARM FasterInvSqrt:  472595.0 usec
ARM FasterInvSqrtN: 426208.0 usec

=============ITERATION 8=============

C InvSqrt:          13734558.0 usec
C FastInvSqrt:      4543793.0 usec
C FastInvSqrtN:     10460907.0 usec
ARM FastInvSqrt:    475708.0 usec
ARM FastInvSqrtN:   491211.0 usec
ARM FasterInvSqrt:  472229.0 usec
ARM FasterInvSqrtN: 426178.0 usec

=============ITERATION 9=============

C InvSqrt:          13731323.0 usec
C FastInvSqrt:      4541687.0 usec
C FastInvSqrtN:     10463074.0 usec
ARM FastInvSqrt:    475982.0 usec
ARM FastInvSqrtN:   490997.0 usec
ARM FasterInvSqrt:  472626.0 usec
ARM FasterInvSqrtN: 425812.0 usec

=============ITERATION 10=============

C InvSqrt:          13734986.0 usec
C FastInvSqrt:      4539368.0 usec
C FastInvSqrtN:     10464600.0 usec
ARM FastInvSqrt:    475647.0 usec
ARM FastInvSqrtN:   491119.0 usec
ARM FasterInvSqrt:  472229.0 usec
ARM FasterInvSqrtN: 426178.0 usec

=============ITERATION 11=============

C InvSqrt:          13733154.0 usec
C FastInvSqrt:      4539001.0 usec
C FastInvSqrtN:     10462067.0 usec
ARM FastInvSqrt:    475799.0 usec
ARM FastInvSqrtN:   491028.0 usec
ARM FasterInvSqrt:  473175.0 usec
ARM FasterInvSqrtN: 425995.0 usec

=============ITERATION 12=============

C InvSqrt:          13736115.0 usec
C FastInvSqrt:      4539886.0 usec
C FastInvSqrtN:     10461670.0 usec
ARM FastInvSqrt:    476532.0 usec
ARM FastInvSqrtN:   491577.0 usec
ARM FasterInvSqrt:  473542.0 usec
ARM FasterInvSqrtN: 426117.0 usec

=============ITERATION 13=============

C InvSqrt:          13739105.0 usec
C FastInvSqrt:      4538819.0 usec
C FastInvSqrtN:     10458740.0 usec
ARM FastInvSqrt:    475708.0 usec
ARM FastInvSqrtN:   491425.0 usec
ARM FasterInvSqrt:  472107.0 usec
ARM FasterInvSqrtN: 426300.0 usec

=============ITERATION 14=============

C InvSqrt:          13738220.0 usec
C FastInvSqrt:      4539276.0 usec
C FastInvSqrtN:     10461700.0 usec
ARM FastInvSqrt:    475982.0 usec
ARM FastInvSqrtN:   491028.0 usec
ARM FasterInvSqrt:  472443.0 usec
ARM FasterInvSqrtN: 426178.0 usec

=============ITERATION 15=============

C InvSqrt:          13739196.0 usec
C FastInvSqrt:      4539886.0 usec
C FastInvSqrtN:     10460235.0 usec
ARM FastInvSqrt:    475892.0 usec
ARM FastInvSqrtN:   491028.0 usec
ARM FasterInvSqrt:  472076.0 usec
ARM FasterInvSqrtN: 426423.0 usec

=============ITERATION 16=============

C InvSqrt:          13734192.0 usec
C FastInvSqrt:      4541199.0 usec
C FastInvSqrtN:     10463134.0 usec
ARM FastInvSqrt:    475769.0 usec
ARM FastInvSqrtN:   491302.0 usec
ARM FasterInvSqrt:  472198.0 usec
ARM FasterInvSqrtN: 426422.0 usec

=============ITERATION 17=============

C InvSqrt:          13734619.0 usec
C FastInvSqrt:      4539948.0 usec
C FastInvSqrtN:     10465515.0 usec
ARM FastInvSqrt:    476013.0 usec
ARM FastInvSqrtN:   490936.0 usec
ARM FasterInvSqrt:  472412.0 usec
ARM FasterInvSqrtN: 426056.0 usec

=============ITERATION 18=============

C InvSqrt:          13731628.0 usec
C FastInvSqrt:      4538849.0 usec
C FastInvSqrtN:     10463654.0 usec
ARM FastInvSqrt:    475860.0 usec
ARM FastInvSqrtN:   491242.0 usec
ARM FasterInvSqrt:  472107.0 usec
ARM FasterInvSqrtN: 426362.0 usec

=============ITERATION 19=============

C InvSqrt:          13736054.0 usec
C FastInvSqrt:      4540039.0 usec
C FastInvSqrtN:     10460693.0 usec
ARM FastInvSqrt:    476043.0 usec
ARM FastInvSqrtN:   490906.0 usec
ARM FasterInvSqrt:  472382.0 usec
ARM FasterInvSqrtN: 430969.0 usec

=============ITERATION 20=============

C InvSqrt:          13733093.0 usec
C FastInvSqrt:      4538940.0 usec
C FastInvSqrtN:     10458465.0 usec
ARM FastInvSqrt:    475922.0 usec
ARM FastInvSqrtN:   490936.0 usec
ARM FasterInvSqrt:  474396.0 usec
ARM FasterInvSqrtN: 426911.0 usec

Abweichung von vrsqrte und vrsqrts zur InvSqrt Funktion:
Totaler Fehler: 1.2217062582956790
Maximaler Fehler: 528.2185058593750000

Abweichung von FastInvSqrtNewton (5F387D4A) zur InvSqrt Funktion:
Totaler Fehler: 0.0008160360802029
Maximaler Fehler: 0.0267238616943359

Abweichung von FastInvSqrtNewton (5F3759DF) zur InvSqrt Funktion:
Totaler Fehler: 0.0005927063492682
Maximaler Fehler: 0.0377101898193359

Durchschnittswerte f�r 20 Iterationen: 

C InvSqrt Zeitspanne:       13735174.6 usec
C FastInvSqrt Zeitspanne:           4539772.0 usec
C FastInvSqrt Newton Zeitspanne:    10461820.8 usec
ARM FastInvSqrt Zeitspanne:         475990.2 usec
ARM FastInvSqrt Newton Zeitspanne:  491108.7 usec
ARM FasterInvSqrt Zeitspanne:       472854.6 usec
ARM FasterInvSqrt Newton Zeitspanne: 426512.3 usec

Geschwindigkeitsverh�ltnisse:

C InvSqrt/ C FastInvSqrt:               3.02552075
ARM FastInvSqrt / ARM FasterInvSqrt:    1.00663111
C InvSqrt / ARM FastInvSqrt:            28.85600584
C InvSqrt / ARM FastInvSqrtN:           27.96769014
C InvSqrt / ARM FasterInvSqrt:          29.04735314
C InvSqrt / ARM FasterInvSqrtN:         32.20346269
C FastInvSqrt / ARM FastInvSqrt:        9.53753360
C FastInvSqrt / ARM FasterInvSqrt:      9.60077802
C FastInvSqrt / C FastInvSqrt-Newton:   0.43393709
ARM FastInvSqrt / ARM FastInvSqrt-Newton:       0.96921557
ARM FasterInvSqrt / ARM FasterInvSqrt-Newton:   1.10865395
ARM FastInvSqrt / ARM FasterInvSqrt-Newton:     1.15145236

Datendurchsatz in MB/s:
C InvSqrt:      9.3191
C FastInvSqrt:  28.1952
C FastInvSqrtN: 12.2350
ARM FastInvSqrt:    268.9131
ARM FastInvSqrtN:   260.6348
ARM FasterInvSqrt:  270.6963
ARM FasterInvSqrtN: 300.1085