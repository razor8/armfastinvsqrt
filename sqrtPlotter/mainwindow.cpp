//Copyright(c) 2013
//Authors: Fabian Wahlster, Steffen Wiewel, Magdalena Papagianni
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com
//License: Read LICENSE.txt located in the root directory

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtCore/qmath.h>
#include <math.h>
#include <QDateTime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(ui->PlottButton,SIGNAL(clicked()),this,SLOT(plott()));
    QObject::connect(ui->InvSqrtButton,SIGNAL(clicked()),this,SLOT(plott_invsqrt()));
    QObject::connect(ui->ScreenshotButton,SIGNAL(clicked()),this,SLOT(screenshot()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::plott(){
    int steps = ui->spinBox->value();
    bool minmax = ui->minMaxCheckBox->isChecked();

    QVector<double> x(steps),y(steps),e(steps);

    double Yk = 0.0;
    double max = 0.0f;

    double s = 1.0/steps;
    int i = 0; double dif = 0.f;
    for(double f = 1.0; i < steps && f < 2.0; f+=s){
        Yk = log2(f);
        x[i] = (f-1.0);
        y[i] = Yk;
        Yk -= (f-1.0);
        if (Yk > max) max = Yk;
        e[i] = Yk;
        dif += Yk;
        ++i;
    }

    dif /= (double)steps;

    //double sigma = 0.045046579168701171875; 5FE6EB3BDB782C00
    double sigma = minmax ? max / 2.0 : dif;

    // 3/2 * (B-sigma)* L ; B = 2^(8-1) L = 2^23 = 8388608.0 for float
    double B = 1023.0, L = 4503599627370496.0;
    double magicd = 1.5*(B - sigma)*L;

    qint64 magic = (qint64)magicd;

    ui->labelRelative->setText(QString("TotalError\t").append(QString::number(dif,'g',16)));
    ui->labelMax->setText(QString("MaxError\t\t").append(QString::number(max,'g',16)));
    ui->labelSigma->setText(QString("Sigma\t\t").append(QString::number(sigma,'g',16)));
    ui->lineEdit->setText(QString::number(magic,16).toUpper());

    ui->CustomPlot->xAxis->setLabel("x");
    ui->CustomPlot->yAxis->setLabel("y");

    ui->CustomPlot->xAxis->setRange(0,1.0f);
    ui->CustomPlot->yAxis->setRange(0,1.0f);

    ui->CustomPlot->addGraph();
    ui->CustomPlot->graph(0)->setData(x,y);
    ui->CustomPlot->graph(0)->setPen(QPen(QColor("blue"), 1));

    ui->CustomPlot->addGraph();
    ui->CustomPlot->graph(1)->setData(x,x);
    ui->CustomPlot->graph(1)->setPen(QPen(QColor("black"), 1));

    ui->CustomPlot->addGraph();
    ui->CustomPlot->graph(2)->setData(x,e);
    ui->CustomPlot->graph(2)->setPen(QPen(QColor("red"), 1));

    ui->CustomPlot->replot();
    //x.clear();y.clear();
}

void MainWindow::screenshot(){
    int steps = ui->spinBox->value();
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QPixmap pm = QPixmap::grabWindow(qApp->desktop()->winId(), this->x()+2, this->y()+2, this->frameGeometry().width()-4, this->frameGeometry().height()-4);
#else
    QPixmap pm = qApp->primaryScreen()->grabWindow(qApp->desktop()->winId(), this->x()+2, this->y()+2, this->frameGeometry().width()-4, this->frameGeometry().height()-4);
#endif
    QString fileName = "steps_" +QString::number(steps) +"_"+ QDateTime::currentDateTime().toString("dd-MM-yy_hh-mm")  +  ".png";
    pm.save(fileName);
}

void MainWindow::plott_invsqrt(){
    bool ok = false;
    qint64 magic = ui->lineEdit->text().toLongLong(&ok,16);
    if(ok == false){
        QFont font = ui->lineEdit->font();
        font.setStrikeOut(true);
        ui->lineEdit->setFont(font);
        return;
    }else{
        QFont font = ui->lineEdit->font();
        font.setStrikeOut(false);
        ui->lineEdit->setFont(font);
    }
    double from = ui->fromDoubleSpinBox->value();
    double to = ui->toDoubleSpinBox->value();
    if(from >= to) return;

    bool newton = ui->newtonCheckBox->isChecked();

    int steps = ui->spinBox->value();
    ui->CustomPlot->clearGraphs();

    QVector<double> x,y,z,e;

    qint64 tmp = 0;int i = 0;
    double s = (to-from)/steps,fast = 0.0,orig = 0.0, error = 0.0,max_error = 0.0,total_error = 0.0;
    for(double f = from; i < steps; f+=s,++i){
        x.append(f);
        orig = 1.0/sqrt(f);
        z.append(orig);
        tmp = *(quint64*)&f;
        tmp = magic - (tmp >> 1);
        fast = *(double*)&tmp;
        if(newton) fast = fast * (1.5 - f*0.5*fast*fast);
        y.append(fast);
        error = fabs(orig-fast);
        e.append(error*10.0);
        if(error > max_error) max_error = error;
        total_error += error;
    }

    total_error /= steps;

    // 3/2 * (B-sigma)* L ; B = 2^(8-1) L = 2^23 = 8388608.0 for float
    double B = 1023.0, L = 4503599627370496.0;

    double sigma = B-(magic/(L*1.5));

    ui->labelRelative->setText(QString("TotalError\t").append(QString::number(total_error,'g',16)));
    ui->labelMax->setText(QString("MaxError\t\t").append(QString::number(max_error,'g',16)));
    ui->labelSigma->setText(QString("Sigma\t\t").append(QString::number(sigma,'g',16)));

    ui->CustomPlot->xAxis->setLabel("x");
    ui->CustomPlot->yAxis->setLabel("y");

    ui->CustomPlot->xAxis->setRange(from,to);
    ui->CustomPlot->yAxis->setRange(0,1.0/sqrt(from));

    ui->CustomPlot->addGraph();
    ui->CustomPlot->graph(0)->setData(x,y);
    ui->CustomPlot->graph(0)->setPen(QPen(QColor("blue"), 1));

    ui->CustomPlot->addGraph();
    ui->CustomPlot->graph(1)->setData(x,z);
    ui->CustomPlot->graph(1)->setPen(QPen(QColor("black"), 1));

    ui->CustomPlot->addGraph();
    ui->CustomPlot->graph(2)->setData(x,e);
    ui->CustomPlot->graph(2)->setPen(QPen(QColor("red"), 1));

    ui->CustomPlot->replot();
}
