//Copyright(c) 2013
//Authors: Fabian Wahlster, Steffen Wiewel, Magdalena Papagianni
//Website: www.singul4rity.com
//Contact: razor@singul4rity.com
//License: Read LICENSE.txt located in the root directory

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
